﻿using System;
using System.Collections.Generic;

namespace NextBiggerTask
{
    public static class NumberExtension
    {
        public static int? NextBiggerThan(int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("123");
            }

            if (number <= 10) 
            {
                return null;
            }

            if (number == int.MaxValue) 
            {
                return null;
            }

            string numbertotext = number.ToString();

            string secondpart = Second(numbertotext);
            if (secondpart == null)
            {
                return null;
            }

            char[] firstpart = First(numbertotext);
            char[] thirdpart = Third(numbertotext);
            List<char> finallist = new List<char>();
            finallist.AddRange(firstpart);
            finallist.AddRange(secondpart);
            finallist.AddRange(thirdpart);
            int number1 = int.Parse(string.Concat(finallist));
            return number1;
        }

        private static string Second(string numbertotext)
        {
            int numberofsymbols = numbertotext.Length;
            int i = numberofsymbols - 1;
            while (i >= 0)
            {
                char symbdigital = numbertotext[i];
                char symbdigitalprevious = numbertotext[i - 1];

                if (symbdigitalprevious < symbdigital)
                {
                    char resultfirst = symbdigital;
                    string resultfinally = $"{resultfirst}";
                    return resultfinally;
                }

                i--;

                if (i == 0)
                {
                    return null;
                }
            }

            return null;
        }

        private static char[] Third(string numbertotext)
        {
            int numberofsymbols = numbertotext.Length;
            int i = numberofsymbols - 1;
            while (i >= 0)
            {
                char symbdigital = numbertotext[i];
                if (i - 1 < 0)
                {
                    List<char> res1 = new List<char>();
                    res1.Add(symbdigital); 
                    char[] ab = res1.ToArray();
                    return ab;
                }

                char symbdigitalprevious = numbertotext[i - 1];
                if (symbdigitalprevious < symbdigital)
                {
                    int k = 1;
                    List<char> res = new List<char>();
                    res.Add(symbdigitalprevious);
                    while (i <= numberofsymbols - 1)
                    {
                        int finish = i + k;
                        if (finish >= numberofsymbols)
                        {
                            res.Sort();
                            char[] a = res.ToArray();
                            return a;
                        }

                        char resultfinally2 = numbertotext[finish];
                        res.Add(resultfinally2);
                        k++;

                        if (finish == numberofsymbols - 1)
                        {
                            res.Sort();
                            char[] a = res.ToArray();
                            return a;
                        }
                    }
                }

                i--;
            }

            char[] array = Array.Empty<char>();
            return array;
        }

        private static char[] First(string numbertotext)
        {
            int numberofsymbols = numbertotext.Length;
            int i = numberofsymbols - 1;
            while (i >= 0)
            {
                char symbdigital = numbertotext[i];
                char symbdigitalprevious = numbertotext[i - 1];
                if (symbdigitalprevious < symbdigital)
                {
                    int k = 0;
                    List<char> res = new List<char>();
                    while (i >= 0)
                    {
                        if (k >= numberofsymbols)
                        {
                            char[] array4 = Array.Empty<char>();
                            return array4;
                        }

                        char resultfinally2 = numbertotext[k];
                        res.Add(resultfinally2);
                        k++;
                        if (k == i - 1)
                        {
                            char[] a = res.ToArray();
                            return a;
                        }
                    }
                }

                i--;
            }

            char[] array = Array.Empty<char>();
            return array;
        }
    }
}
